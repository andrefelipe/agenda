import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    
    //interagindo com usuário
    System.out.println("Digite o nome da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umNome = entradaTeclado;
    
    System.out.println("Digite o telefone da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umTelefone = entradaTeclado;
    
    //adicionando uma pessoa na lista de pessoas do sistema
    Pessoa umaPessoa = new Pessoa(umNome, umTelefone);
    String mensagem = umControle.adicionar(umaPessoa);

    //conferindo saída
    System.out.println("=================================");
    System.out.println(mensagem);
    System.out.println("=)");

  }

}
